﻿using System.IO;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerManager : MonoBehaviour {
	public VideoPlayer vdoPlayer;
	public bool isLoopVDO;
	public delegate void VdoDispatcher();
    public event VdoDispatcher onVdoEnd;
    public GameObject vdoLayer;

    void Start() {
        vdoPlayer.isLooping = (isLoopVDO) ? true: false;
        vdoPlayer.loopPointReached += EndVdo;
        vdoLayer.SetActive(false);
    }

    public void PlayVdoDirect(string path){
        if(!File.Exists(path)) return; //Not have file in there.
        vdoLayer.SetActive(true);
        if (vdoPlayer.isPlaying) vdoPlayer.Stop();
        vdoPlayer.url = path;
        vdoPlayer.frame = 0;
        vdoPlayer.Play();
    }

    void EndVdo(VideoPlayer vp) {
        //Dispatch vdo is ended.
        if(onVdoEnd != null) onVdoEnd();
       
        if(!isLoopVDO) { 
            if (vp.isPlaying) {
                vp.Stop();                
            }
            Clear();
        }
        vdoLayer.SetActive(false);
    }

    public void Pause() {
        if (vdoPlayer.isPlaying) vdoPlayer.Pause();
    }

    public void Resume() {
        if (vdoPlayer.isPaused) vdoPlayer.Play();
    }

    private void Play(VideoPlayer _vp) {
        _vp.Play();
    }
    
    public void Clear() {
        vdoPlayer.targetTexture.Release();
        vdoPlayer.targetTexture.DiscardContents();
    }
    
}
