using UnityEngine;

public class InputReader : MonoBehaviour {
    public delegate void ValueDispathcer(string txt);
    public event ValueDispathcer OnGetValue;
    private string inputValue;
    private float updateTime;
    private int gabageCount; //Amount of number blank data.
    void Start() {
        inputValue = "";
        updateTime = 0;
        gabageCount = 0;
    }

    void Update() {
        updateTime += Time.deltaTime;
        if (!string.IsNullOrEmpty(Input.inputString)) {
            inputValue += Input.inputString;
        }

        if (inputValue != "") {           
            if (inputValue.Length < 8) {
                gabageCount++;
                if(gabageCount >= 10)  {
                    inputValue = "";
                    gabageCount = 0;
                }
                Debug.Log("Reader ID too short. === " + inputValue);
                return;
            } else { 
                if (updateTime < 1) { //reader cooldown 
                    inputValue = "";
                    Debug.Log("Time spent not enough.");
                    return;
                } else { //reader ready.
                    updateTime = 0;                    
                    inputValue = inputValue.Substring(0, 8).ToUpper()+"";
                    if(OnGetValue!=null) OnGetValue(inputValue); //Dispatch here.
                    Debug.Log("inputValue === " + inputValue);
                }
                inputValue = "";
                gabageCount = 0;
            }
        }
    }
}