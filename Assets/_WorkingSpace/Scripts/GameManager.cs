using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour {
    public int[] listStep;
    public Sprite[] itemImageAll;
    public Image oldMan1, oldMan2;
    public Text debugTxt;
    public Sprite[] imagesOldman;
    public Sprite[] imagesDoctor;

    public Image doctor;
    public TextMeshProUGUI doctorSaidTxt;
    public GameObject doctorBubbleText;
    public Text countdownTxt;
    public GameObject countDownLayer;
    public GameObject layerForCountDown;

    public GameObject drDescription;
    public GameTimerClass gameTimer;
    public Image fadeObject;

    public float delayForInfo = 7f;
    public BoneStepController boneControl;

  //  public int maxCombo;

    public GameObject objQuest;
    public TextMeshProUGUI objTxt;
    public Image[] imageAllQuest;

    public Image itemScan;
    public GameObject boxScan;
    public Animator drAnimator;

    public ItemEventDispatcher itemEventDispatcher;
    public ItemEventDispatcher itemEventDispatcherFailed;

    int currentCombo;

    Global global;

    public bool isPlay {
        set; get;
    }

    void Start(){
        isPlay = false;
        global = Global.ins;
        global.SetDebugTextByScene(debugTxt);

        gameTimer.OnTimeup += OnTimeUp;
        gameTimer.SetDefaultParam((int)global.jsonReader.GameTime);

        objQuest.SetActive(false);

        layerForCountDown.SetActive(true);

     //   maxCombo = global.jsonReader.MaxCombo;
        countdownTxt.text = "";
        objTxt.text = global.jsonReader.listGroupData[(listStep[currentQuest-1])].itemName;
        Invoke("StartGamePlay", 10f);
    }

    public void WinGame() {
        EndGame(true);
    }

    public void ShowWelcome() {
        isPlay = false;
        currentCombo = 0;
    }

    private void StartCountDown(){
        drDescription.transform.DOLocalMoveX(1800, .35f);
        countDownLayer.SetActive(true);
        count = 4;
        UpdateCountText();
    }

    private void StartGamePlay() {
        countdownTxt.text = "";
        countDownLayer.SetActive(false);
        isPlay = true;
        //ShowQuestID(GetInverseCurrentQuest(currentQuest));

        layerForCountDown.SetActive(false);
        gameTimer.StartCountDown(0);
        objQuest.SetActive(true);

        drAnimator.SetTrigger("Start");
    }

    int GetInverseCurrentQuest(int curNum) {
        switch (curNum) {
            case 1:
                return 3;
            case 3:
                return 1;
            default:
                return curNum;
        }
    }

    public void GenNewQuest(string s){        
        itemToFind = s;
    }
    string itemToFind;
    int currentQuest = 1;

    public void CheckID(string s) {
        if (isPlay) {
            string ss = global.GetItemNameByKey(s);
            int currentKeyReadedIndex = -1;
            JsonGroupData groupData;
            for (int i = 0; i < global.jsonReader.listGroupData.Count; i++) {
                if (global.jsonReader.listGroupData[i].itemName == ss) {
                    currentKeyReadedIndex = i;
                    groupData = global.jsonReader.listGroupData[i];
                    break;
                }
            }
            
                
            if (currentKeyReadedIndex == listStep[currentQuest - 1]) {
                OnRfidScan(currentKeyReadedIndex, true);
                isPlay = false;
                global.soundSystem.PlaySFX(SoundSystem.SfxName.Correct);
                doctorSaidTxt.text = "ถูกต้อง<br>เก่งมากเลย";
                ShowDoctor(true);
                currentQuest++;
                
                DOVirtual.DelayedCall(1f, ()=>objTxt.text = global.jsonReader.listGroupData[(listStep[currentQuest-1])].itemName);
                if (currentQuest > listStep.Length) {
                    objTxt.text = "";
                    gameTimer.StopTimer();
                }
                DOVirtual.DelayedCall(1.5f,()=> ShowQuestID(GetInverseCurrentQuest(currentQuest)));
            } else {
                OnRfidScan(currentKeyReadedIndex, false);
                global.soundSystem.PlaySFX(SoundSystem.SfxName.False);
                ShowDoctor(false);
                doctorSaidTxt.text = "ไม่ถูกต้อง<br>ลองอีกครั้งนะ";
                OldManEmotion();
            }
        }
    }

    private void OnRfidScan(int index, bool isCorrect) {
        if (isCorrect) {
            itemEventDispatcher.s_renderer.sprite = itemImageAll[index];
            itemEventDispatcher.gameObject.SetActive(true);
        } else {
            ////////////////////////////////////////////////////////////////
            itemEventDispatcherFailed.s_renderer.sprite = itemImageAll[index];
            itemEventDispatcherFailed.gameObject.SetActive(true);
        }

    }

    public GameObject[] emotions;
    private void OldManEmotion() {
        int rnd = Random.RandomRange(0, emotions.Length);
        emotions[rnd].SetActive(true);
    }
    public GameObject timeUpAnimation;
    public GameObject boneBoxAnimation;
    public AudioSource mainBgmSource;
    private void OnTimeUp() {
        isPlay = false;
        mainBgmSource.volume = 0;
        timeUpAnimation.SetActive(true);
        boneBoxAnimation.SetActive(false);
        DOVirtual.DelayedCall(4f, ()=>EndGame(false));
    }

    private void EndGame(bool isPlayerWin) {
        global.isPlayerWin = isPlayerWin;
        if(!isPlayerWin){
            fadeObject.DOFade(1, .5f).OnComplete(() => {
                global.LoadSceneAt(1);
            });
        }else{
            fadeObject.DOFade(1, .5f).OnComplete(() => {
                global.LoadSceneAt(5);
            });
        }
    }

    void ShowQuestID(int id) {
        boneControl.NextAnim();      
    }

    private void ShowDoctor(bool isTrue) {
        doctor.sprite = imagesDoctor[Convert.ToInt32(isTrue)];
        doctorBubbleText.SetActive(true);
        if(isTrue){
            doctor.transform.DOLocalMoveX(-640, 0.5f).OnComplete(
                ()=> { 
                    DOVirtual.DelayedCall(2.5f, ()=> {
                        doctorBubbleText.SetActive(false);
                        doctor.sprite = imagesDoctor[2];
                    });
                    doctor.transform.DOLocalMoveX(-1853, 0.5f).SetDelay(delayForInfo+.75f);
                }); 
        }else{
            doctor.transform.DOLocalMoveX(-640, 0.5f).OnComplete(
                ()=> doctor.transform.DOLocalMoveX(-1853, 0.5f).SetDelay(1f));
        }                
    }

    private void OldmanActive(bool isTrue, bool isRepeat=true) {
        if(isTrue) { 
            oldMan2.transform.DOLocalMoveX(800, 1).SetDelay(1.5f).OnComplete(()=> { 
                if(isRepeat) {
                    oldMan2.color = new Color(oldMan2.color.r, oldMan2.color.g, oldMan2.color.b, 0);
                    oldMan2.transform.localPosition = Vector3.zero; 

                    ShowTextDetail(true);
                   
                    oldMan1.DOFade(1f, .5f).SetDelay(delayForInfo-1.5f); 
                }else{
                    ShowTextDetail(true, true);                   
                }
            });
        } else {
            oldMan1.DOFade(1f, .5f); 
            oldMan2.DOFade(0f, .5f);
        }
    }

    private void ShowTextDetail(bool isShow, bool isEndedGame=false) {
        if(isShow){
            objTxt.DOFade(1, .3f);
         //   txtDetail.DOFade(1, .3f).SetDelay(.3f);
            DOVirtual.DelayedCall(delayForInfo-1.5f, ()=> ShowTextDetail(false, isEndedGame));
        } else {
            if(isEndedGame){
                objTxt.DOFade(0, .3f).SetDelay(.3f).OnComplete(()=> {
                    OnEndRound(true);
                    gameTimer.StopTimer();
                });
            }else{
                objTxt.DOFade(0, .3f).SetDelay(.3f);
            }
            //txtDetail.DOFade(0, .3f);
        }
    }

    public void AddItemBox() {
        drAnimator.SetTrigger("GetRFID");
    }

    private void OnEndRound(bool isPlayerwin) {
        global.isPlayerWin = isPlayerwin;
        Debug.Log("Change to end scene.");
        global.LoadSceneAt(4);
    }

    public string currentKey{
        get; set;
    }

#region Text Countdown
    int textSize;
    int count;
    void UpdateCountText() {
        textSize = 400;
        count--;        
        if (count > 0) {
            global.soundSystem.PlaySFX(SoundSystem.SfxName.Beep);
            countdownTxt.text = count.ToString();
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(UpdateCountText);
        } else {
            global.soundSystem.PlaySFX(SoundSystem.SfxName.BeepLong);
            countdownTxt.text = "GO!";
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(StartGamePlay);
        }
    }

    void UpdateTextSize() {
        countdownTxt.fontSize = textSize;
    }
#endregion

}
