using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class Totorial : MonoBehaviour {
    public Text debugTxt;
    public float timeDelay;
    public Image fadeObject;
    void Start() {
        Global.ins.SetDebugTextByScene(debugTxt);
        Invoke("GotoGamePlay", timeDelay);
    }

    void GotoGamePlay() {
        fadeObject.DOFade(1, 0.5f).OnComplete(()=> Global.ins.LoadSceneAt(3));
    }
}
