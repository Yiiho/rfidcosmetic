using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class Global : MonoBehaviour {
    public static Global ins;
    Text debugTxt;
    public SoundSystem soundSystem;
    public InputReader inputReader;
    public JsonReader jsonReader;
    public GameManager gameManager;
    public TestPlay testPlay;
    public bool isPlayerWin;
    float timerCount = 0;
    float maxInterval;
    bool isDebugMode;

    void Awake() {
        if(ins == null) ins = this;
        DontDestroyOnLoad(this.gameObject);
    }

    void Start() {
        Cursor.visible = isDebugMode = false;
        Invoke("SetFullScreen", 2.5f);        
        inputReader = GetComponent<InputReader>();
        jsonReader = GetComponent<JsonReader>();
        jsonReader.ReadAllData();

        Invoke("GotJson", 3f);
    }

    void SetFullScreen() {
        Screen.fullScreen = true;
    }

    IEnumerator CheckIntervalTime() {
        yield return new WaitForSeconds(.5f);
        timerCount += .5f;
        if(timerCount >= maxInterval) {
            if(SceneManager.GetActiveScene().buildIndex != 1) LoadSceneAt(1); //Go to main scene.
            timerCount = 0;
        }
        StartCoroutine(CheckIntervalTime());
    }

    void GotJson() {
        LoadSceneAt(1); // go to main scene. ///////////////////////////////////////////////////////////
        print("GotJson");
        maxInterval = jsonReader.IntervalTimer;
        inputReader.OnGetValue += MatchingValue;

        //   keyToStart = jsonReader.keyAndName.FirstOrDefault(x => x.Value == "Start").Key;

        keyToStart = jsonReader.listGroupData[0].itemName;


        StartCoroutine(CheckIntervalTime());
    }

    public string keyToStart{
        get; private set;
    }

    public bool CompareIdAndKeyname(string rfidInput, string keyItemName) {
        return (jsonReader.dicID[rfidInput] == keyItemName);
    }

    public string GetItemNameByKey(string rfidInput) {
        return jsonReader.dicID[rfidInput];
    }

    public JsonGroupData GetGroupData(string rfidInput) {
        string s = GetItemNameByKey(rfidInput);
        foreach(JsonGroupData jsonGroupData in jsonReader.listGroupData) {
            if (s == jsonGroupData.itemName) return jsonGroupData;
        }
        return new JsonGroupData();
    }

    void MatchingValue(string s){       
        timerCount = 0;
        if(gameManager){
            gameManager.CheckID(s);
            SetTextDebug("Current ID : "+ gameManager.currentKey + 
                "\n Revieved RFID : " + s);
        } else if (testPlay) {
            testPlay.CheckID(s);
        }    
    }

    public void SetDebugTextByScene(Text txt) {
        debugTxt = txt;
        debugTxt.gameObject.SetActive(isDebugMode);
        if(gameManager) {
            SetTextDebug("Current ID : "+ gameManager.currentKey); 
        }
    }

    public void SetTextDebug(string s) {
        if(debugTxt!=null){
            debugTxt.text = s;
        } /*else {
            debugTxt = GameObject.Find("DebugTxt").GetComponent<Text>();
        }*/
    }

    public void LoadSceneAt(int id) {
        DOTween.KillAll();
        SceneManager.LoadScene(id);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        //   Debug.Log("OnSceneLoaded: " + scene.name);
        //   Debug.Log(mode);
        if (scene.buildIndex == 4) {
            gameManager = GameObject.FindObjectOfType<GameManager>();
         //   gameManager.maxCombo = jsonReader.MaxCombo;
        } else if (scene.buildIndex == 3) { 
            testPlay = GameObject.FindObjectOfType<TestPlay>();
        } else {
            gameManager = null;
            testPlay = null;
        }
    }

/*
    private void Update() {
        if(Input.GetKeyUp(KeyCode.M)) {
            if(debugTxt){                
                debugTxt.gameObject.SetActive(Cursor.visible = isDebugMode = !isDebugMode);
            }
            
        }
    }
*/
}
