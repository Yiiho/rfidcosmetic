﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class EndGame : MonoBehaviour {
    void Start() {
        Invoke("GoToMain", 6f);
    }

    void GoToMain() {
        Global.ins.LoadSceneAt(1);
    }
}
