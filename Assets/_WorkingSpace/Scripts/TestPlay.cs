using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class TestPlay : MonoBehaviour {
    public Image img;
    public Sprite[] itemAll;
    public Image header;
    public Sprite[] headerSpr;
    public TextMeshProUGUI textName, textData;
    //public GameObject boneBoxGo; 
    public Image fadeObject;
    public Image dr;
    public Sprite[] drSprite;
    public GameObject[] timelines;

    Global global;    
    int currentKeyIndex;
    Dictionary<string, string> dictValue;
    JsonGroupData groupData;

    void Start() {
        //fadeObject.gameObject.SetActive(true);
        global = Global.ins;
        timelines[1].SetActive(false);
        //fadeObject.DOFade(0, .3f);
    }

    public void CheckID(string s) {
        string ss = global.GetItemNameByKey(s);
        currentKeyIndex = -1;
        for (int i=0; i< global.jsonReader.listGroupData.Count; i++) {
            if(global.jsonReader.listGroupData[i].itemName == ss) {
                currentKeyIndex = i;
                groupData = global.jsonReader.listGroupData[i];
                break;
            }
        }
        DrSpriteSwap();

        if(currentKeyIndex != -1) OnRfidScan();
       
        if(currentKeyIndex > 0) {
            if(currentKeyIndex>=4){
                header.sprite = headerSpr[1];
            }else{
                header.sprite = headerSpr[0];
            }
           // dictValue = global.jsonReader.GetDataByPointer(currentKeyIndex, true, true, true);
            img.DOFade(0, .25f).OnComplete(() => {
                img.sprite = itemAll[currentKeyIndex - 1];
                img.DOFade(1, .25f);
            });
            textName.DOFade(0, .25f).OnComplete(() => {
                textName.text = groupData.itemName;
                textName.DOFade(1, .25f);
            });
            textData.DOFade(0, .25f).OnComplete(() => {
                textData.text = groupData.data;
                textData.DOFade(1, .25f);
            });
            if (!timelines[1].activeSelf) {
                timelines[0].SetActive(true); 
            //    boneBoxGo.SetActive(false);
             //   boneBoxGo.SetActive(true);
                timelines[1].SetActive(true);
            }
        } else if(currentKeyIndex == 0) { //key start
            if (timelines[1].activeSelf) {
                fadeObject.DOFade(1, .5f).OnComplete(() => global.LoadSceneAt(4));
            }
        }
    }

    public AudioSource audS;
    private void OnRfidScan() {
        audS.Play();
    }

    private void DrSpriteSwap() {
        dr.DOFade(0.5f, .2f).OnComplete(() => {
            dr.sprite = drSprite[1];
            dr.DOFade(1f, .2f).OnComplete(() => {
                dr.DOFade(0.5f, .2f).SetDelay(1f).OnComplete(() => {
                    dr.sprite = drSprite[0];
                    dr.DOFade(1f, .2f);
                });
            });
        });
    }
}
