using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class AnimationSceneManager : MonoBehaviour {
    public ObjectAnimation[] objAnims;

    void Start()
    {
        DOVirtual.DelayedCall(2f,()=> ActiveStep(0));
    }

    public void ActiveStep(int id) {
        switch(id){
            case 0:
              //  SetSceneFirst();
                break;
            case 1:
            //    SetSceneTutorial();
                break;
        }
    }
 /*
    private void SetSceneFirst() {
        objAnims[0].goContainer.SetActive(true);
       
        bg1_2.color = bg1_3.color = new Color(bg1_2.color.r,bg1_2.color.g,bg1_2.color.b, 0);
        bg1_2.transform.rotation = bg1_3.transform.rotation = new Quaternion(0,0,0,0); 
        bg1_2.transform.localPosition = bg1_3.transform.localPosition = new Vector2(1136, 0);
        bg1_2.transform.DOLocalMoveX(0, .6f).OnComplete(
            ()=> bg1_3.transform.DOLocalMoveX(0, .6f));
        bg1_2.DOFade(1, .6f).OnComplete(()=> bg1_3.DOFade(1, .6f));
    }

    private void SetSceneTutorial() {
        bg1_2.transform.DOLocalMoveX(1600, 1.5f).OnComplete(DoScreenTutorial);
        bg1_2.DOFade(0, 1.5f).OnUpdate(()=> { bg1_3.color = bg1_2.color; });
    }

    private void DoScreenTutorial() {
        bg1_2.transform.DOLocalRotate(new Vector3(0,0,180), 0.01f);
        bg1_2.transform.localPosition = new Vector2(-1600, 0);
        bg1_2.transform.DOLocalMoveX(0, 1.5f).OnComplete(DoScreenTutorial);
        bg1_2.DOFade(0, 1.5f).OnUpdate(()=> { bg1_3.color = bg1_2.color; });
    }*/
}

[System.Serializable]
public struct ObjectAnimation {
    public GameObject goContainer;
    public Image[] imgs;
    public TextMeshPro[] txts;
}
