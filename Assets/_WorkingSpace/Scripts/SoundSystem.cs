using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSystem : MonoBehaviour {
    AudioSource audioSource;
    [Range(0f, 2f)]
    public float volume = 1f;
    public AudioClip[] fxClips;
    //public AudioClip[] bgClips;
    public enum SfxName { False, Correct, Beep, BeepLong }

    public void PlaySFX(SfxName sName) {
        if (audioSource == null) audioSource = Camera.main.GetComponent<AudioSource>();
        audioSource.PlayOneShot(GetClipFromName(sName));
    }
    /*
    public void PlayBGM(int id) {
        if (audioSource == null) audioSource = Camera.main.GetComponent<AudioSource>();
        audioSource.clip = bgClips[id];
        audioSource.Play();
    }
    */
    AudioClip GetClipFromName(SfxName sName) {
        switch (sName) {
            case SfxName.False:
                return fxClips[0];
            case SfxName.Correct:
                return fxClips[1];
            case SfxName.Beep:
                return fxClips[2];
            case SfxName.BeepLong:
                return fxClips[3];
        }
        return fxClips[0];
    }


}
