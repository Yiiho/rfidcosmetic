using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneStepComponent : MonoBehaviour {
    BoneStepController boneStepController;
    void Start() {
        boneStepController = gameObject.GetComponentInParent<BoneStepController>();
    }

    public void GoToNextAnim() {
        boneStepController.NextAnim();
    }

    public void ResetPlayable() {
        boneStepController.ResetPlayableStatus();
    }

}
