using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Main2 : MonoBehaviour {
    public Image fadeObject;
    public Text debugTxt;
    public GameObject soundScan;

    void Start() {
        Global.ins.SetDebugTextByScene(debugTxt);
        Cursor.visible = false;
        Invoke("WaitForSignal", 2f);
    }

    void WaitForSignal(){
        Global.ins.inputReader.OnGetValue += MatchingValue;
    }

    void MatchingValue(string s){
        Global.ins.SetTextDebug("Recieved RFID : " + s);
        print(Global.ins.keyToStart);

        if (Global.ins.CompareIdAndKeyname(s, Global.ins.keyToStart)){
            soundScan.SetActive(true);
            Global.ins.inputReader.OnGetValue -= MatchingValue;
            fadeObject.DOFade(1, 0.5f).OnComplete(() => Global.ins.LoadSceneAt(2));
        }             
    }

}
