using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEventDispatcher : MonoBehaviour {
    public SpriteRenderer s_renderer;
    public void ToShowFaceItem() {
        if (Global.ins.gameManager) {
            Global.ins.gameManager.AddItemBox();
        }
    }

    public void RemoveObject() {
        Destroy(gameObject);
    }

    public void Deactive() {
        this.gameObject.SetActive(false);
    }
}
