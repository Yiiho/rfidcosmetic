using UnityEngine;
using UnityEngine.UI;

public class CustomSetThaiFont : MonoBehaviour {
    public string strictText;
    Text txt;
    void Start() {
        txt = GetComponent<Text>();        
        if(!string.IsNullOrEmpty(strictText)) SetText(strictText);
    }

    public void SetText(string s) {
        txt.text = ThaiFontAdjuster.Adjust(s);
    }
   
}
