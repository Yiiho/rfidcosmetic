using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Main : MonoBehaviour {
    public InputReader inputReader;
    public JsonReader jsonReader;
    public VideoPlayerManager vpm;
    public GameManager gameManager;
    public Text debugLayer;
    float timerCount = 0;
    float maxInterval;

    void Start() {        
        jsonReader.ReadAllData();
        Invoke("GotJson", 3f);

        debugLayer.gameObject.SetActive(Cursor.visible = false);
        StartCoroutine(CheckIntervalTime());
    }

    
    IEnumerator CheckIntervalTime() {
        yield return new WaitForSeconds(.5f);
        timerCount += .5f;
        if(timerCount >= maxInterval) {
            gameManager.ShowWelcome();
            timerCount = 0;
        }
        StartCoroutine(CheckIntervalTime());
    }

    void GotJson() {
        print("GotJson");
        maxInterval = jsonReader.IntervalTimer;
        inputReader.OnGetValue += MatchingValue;
      //  gameManager.maxCombo = jsonReader.MaxCombo;
    }
    
    void MatchingValue(string s){
        gameManager.CheckID(s);
        timerCount = 0;
        debugLayer.text = "Current ID : "+ gameManager.currentKey + 
            "\n Revieved RFID : " + s;     
    }

    private void PlayVDO(string s) {
        vpm.PlayVdoDirect(s);
    }

    public void PlayerCorrect() {        
        Invoke("GenNewKey", .5f);
    }
    

    private void Update() {
        if(Input.GetKeyUp(KeyCode.M)){
            if(debugLayer.gameObject.activeSelf){
                debugLayer.gameObject.SetActive(Cursor.visible = false);
            } else {
                debugLayer.gameObject.SetActive(Cursor.visible = true);
            }
        }
    }
}
