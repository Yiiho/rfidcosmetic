using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneStepController : MonoBehaviour {
    public GameObject[] listAll;
    public GameManager manager;
    int pointer = 0;
    private void Start() {
        for(int i=1; i < listAll.Length; i++) {
            listAll[i].SetActive(false);
        }
    }

    public void NextAnim() {
        listAll[pointer].SetActive(false);
        pointer++;
        if (pointer == listAll.Length) {
            manager.WinGame();
            return;
        } else {
            listAll[pointer].SetActive(true);
        }
        //manager.isPlay = true;
    }

    public void ResetPlayableStatus() {
        manager.isPlay = true;
    }

}
