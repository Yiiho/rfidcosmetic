using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;

public class JsonReader : MonoBehaviour {
    public delegate void ReadJson();
    public event ReadJson OnJsonReaded;
    public string jsonFileName;

    public List<JsonGroupData> listGroupData = new List<JsonGroupData>();
    public Dictionary<string, string> dicID = new Dictionary<string, string>();
    List<List<string>> keys = new List<List<string>>();
    List<string> names = new List<string>();
    List<string> datas = new List<string>();
 
    public float IntervalTimer { get; private set; }
    public float GameTime { get; private set; }
    public int MaxCombo { get; private set; }
    public void ReadAllData() {
        string dir = Application.dataPath + "/../" + jsonFileName;
        LoadJsonFile(dir);
    }

    private void LoadJsonFile(string path) {
        if (File.Exists(path)) {
            string s = File.ReadAllText(path);
            DoSimpleJson(s);
        } else {
            Debug.Log("File Not Exists at "+path);
        }
    }

    private void DoSimpleJson(string s) {
        //Read JSON.
        JSONNode jsonNode = SimpleJSON.JSON.Parse(s);
        //Add fixed parameter struture.
        IntervalTimer = float.Parse(SplitString(jsonNode["IntervalTime"].ToString()));
        GameTime = float.Parse(SplitString(jsonNode["GameTime"].ToString()));
        MaxCombo = int.Parse(SplitString(jsonNode["MaxComboPerRound"].ToString()));

        for (int i = 0; i < jsonNode["Items"].Count; i++) {
            List<string> dumList = new List<string>();
            JsonGroupData gData = new JsonGroupData();

            //Add struct database.
            gData.itemName = SplitString(jsonNode["Items"][i]["name"].ToString());
            gData.data = SplitString(jsonNode["Items"][i]["data"].ToString());
            listGroupData.Add(gData);
            //Add all RFID paring each key.
            for (int j = 0; j < jsonNode["Items"][i]["id"].Count; j++) {
                dicID.Add(jsonNode["Items"][i]["id"][j], gData.itemName);
            }
        }
        //Json read completed then dispatch here.
        if (OnJsonReaded!=null) OnJsonReaded();
    }

    public bool CompareIDandKeyname(string rfidInput, string keyItemName) {
        return (dicID[rfidInput] == keyItemName);
    }

    public string GetNameByKey(string s){
        int index;
        foreach (List<string> l in keys) {
            index = l.IndexOf(s);
            if(index != -1) {
                return l[index];
            }
        }
        return "";
    }

    public int GetIndexByKey(string s) {
        int index;
        foreach (List<string> l in keys) {
            index = l.IndexOf(s);
            if (index != -1) {
                return index;
            }
        }
        return -1;
    }
    /*
    public Dictionary<string, string> GetDataByPointer(int pointer, bool reqKey, bool reqName, bool reqData) {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        if (reqKey) {
            dict.Add("key", keys[pointer][pointer]);
        }
        if (reqName) dict.Add("name", names[pointer]);
        if (reqData) dict.Add("data", datas[pointer]);
        return dict;
    }
    */
    /*
    public string GetNamebyIndex(int id) {
        return names[id];
    }
    */
    string SplitString(string s) {
        s = s.Replace(@"\", "/");
        s = s.Replace("\"", "");
        return s;
    }

}

[System.Serializable]
public struct JsonGroupData {
    public string itemName;
    public string data;
}
