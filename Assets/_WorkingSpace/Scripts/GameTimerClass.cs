using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameTimerClass : MonoBehaviour {
    public delegate void Timeup();
    public event Timeup OnTimeup;

    Text txt;
    int _targetNumber;
    int _startNumber;
    int _currentNumber;

    private void Start() {
        txt = GetComponent<Text>();
    }

    public void SetDefaultParam(int startNumber) {
        _startNumber = startNumber;
        txt.text = "" + startNumber;
    }

    public void StartCountDown(int targetNumber) {
        _targetNumber = targetNumber;
        _currentNumber = _startNumber;
        StartCoroutine(CountDown());
    }

    IEnumerator CountDown() {
        yield return new WaitForSeconds(1f);
        _currentNumber--;
        txt.text = "" + _currentNumber;
        if (_currentNumber <= _targetNumber) {
            if (OnTimeup != null) OnTimeup();
            yield break;
        }
        StartCoroutine(CountDown());
    }

    public void StatCountUp(int targetNumber) {
        _targetNumber = targetNumber;
        StartCoroutine(CountUp());
    }

    IEnumerator CountUp() {
        yield return new WaitForSeconds(1f);
        _currentNumber++;
        txt.text = "" + _currentNumber;
        if (_currentNumber >= _targetNumber) {
            if (OnTimeup != null) OnTimeup();
            yield break;
        }
        StartCoroutine(CountUp());
    }

    public void StopTimer() {
        StopAllCoroutines();
    }

    string GetDynamicDigit(int value, int length) {
        string s = value.ToString();
        while(s.Length < length) {
            s = "0" + s;
        }
        return s;
    }
}
